# wifi_psk_dump

This is a small tool that prints any Wifi PSKs stored in NetworkManager on the currently logged in Linux desktop.

## Development

The packages for the virtual environment are listed in requirements.txt

On Fedora 30, to establish that virtual environment, I had to install

* `python3-devel`
* `dbus-devel`
* `glib2-devel`

from the system package manager.

Once those are installed:

```
python3 -mvenv venv-wifi_psk_dump
source venv-wifi_psk_dump/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Authors

* **Geoff Beier** - *Initial work* - [Repository](https://gitlab.com/geoffbeier)

