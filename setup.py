#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages


setup(
    name='wifi_psk_dump',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click'
    ],
    entry_points={
        'console_scripts': ['wifi_psk_dump=wifi_psk_dump.cli:cli']
    }
)
