#!/usr/bin/env python
# -*- coding: utf-8 -*-

import NetworkManager
import click

# if this gets generalized into a more useful tool, this should probably return a structure that
# gets handed off to a pretty printer of some sort.
# For now, this is the quickest way to wrap it into an installable script.

def print_psks(ids = None, print_open = False):
    for conn in NetworkManager.Settings.ListConnections():
        settings = conn.GetSettings()
        conn_id = settings['connection']['id']
        conn_type = settings['connection']['type']
        conn_found = False
        if ids is not None:
            conn_found = conn_id in ids
        if ids is not None and not conn_found:
            continue
        if (conn_type == "802-11-wireless"):
            if 'security' in settings['802-11-wireless'] and \
                    settings['802-11-wireless']['security'] == '802-11-wireless-security':
                security_settings = settings['802-11-wireless-security']
                if (security_settings['key-mgmt'] == 'wpa-psk'):
                    secrets = conn.GetSecrets()['802-11-wireless-security']
                    if ids is None or conn_found:
                        try:
                            click.secho(f"WPA PSK for {conn_id} = {secrets['psk']}", fg='bright_green', bold=True)
                        except KeyError as e:
                            click.secho(f"{conn_id} is configured for wpa-psk but no key is stored.", fg='bright_red', bold=True)
            elif 'security' in settings['802-11-wireless']:
                print("[Connection %s]: Unexpected value for security key in dictionary: %s" % \
                      (conn_id, settings['802-11-wireless']['security']))
            else:
                if print_open:
                    if ids is None or conn_found:
                        click.secho(f'{conn_id} is open wifi', fg='yellow')
