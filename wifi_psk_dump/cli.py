#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. currentmodule:: wifi_psk_dump.cli
.. moduleauthor:: Geoff Beier <geoff@tuxpup.com>
This is the entry point for the command-line interface (CLI) application.  It
can be used as a handy facility for running the task from a command line.
.. note::
    To learn more about Click visit the
    `project website <http://click.pocoo.org/5/>`_.  There is also a very
    helpful `tutorial video <https://www.youtube.com/watch?v=kNke39OZ2k0>`_.
    To learn more about running Luigi, visit the Luigi project's
    `Read-The-Docs <http://luigi.readthedocs.io/en/stable/>`_ page.
"""

import logging
import string
import click
from wifi_psk_dump import print_psks, __version__
import sys, os

LOGGING_LEVELS = {
    0: logging.NOTSET,
    1: logging.ERROR,
    2: logging.WARN,
    3: logging.INFO,
    4: logging.DEBUG
}  #: a mapping of `verbose` option counts to logging levels

@click.command()
@click.option('--verbose', '-v', count=True, help="Enable verbose output. (Prints the names of saved open networks, also.)")
@click.option('--version', '-V', is_flag=True)
@click.argument('ssid', metavar="<ssid> ...", nargs=-1)
def cli(verbose: int, ssid: [str], version: bool):
    """
    This script dumps stored Wifi PSKs from NetworkManager.
    If SSIDs are specified as arguments, only PSKs for the specified SSIDs are dumped.
    """
    if version:
        click.echo(f"{os.path.basename(sys.argv[0])} version {__version__}")
        sys.exit(0)

    # more -v => more verbosity
    if verbose > 0:
        logging.basicConfig(
            level=LOGGING_LEVELS[verbose]
            if verbose in LOGGING_LEVELS
            else logging.DEBUG
        )
    if(len(ssid) == 0):
        print_psks(None, verbose > 0)
    else:
        print_psks(ssid, verbose > 0)


if __name__ == "__main__":
    cli()
