#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. currentmodule:: wifi_psk_dump
.. moduleauthor:: Geoff Beier <geoff@tuxpup.com>
This is a small tool that prints any Wifi PSKs stored in NetworkManager on the currently logged in Linux desktop.
"""
from .version import __version__, __release__
from .pskdumper import print_psks
