#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. currentmodule:: wifi_psk_dump.version
.. moduleauthor:: Geoff Beier <geoff@tuxpup.com>
This module contains project version information.
"""

__version__ = '0.1.0'  #: the working version
__release__ = '0.1.0'  #: the release version

